<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            try {
                $this->db = new PDO("mysql:host=localhost;dbname=oblig1;charset=utf8", "root", "");
            } catch (PDOException $e) {
                echo $e->getMessage() . PHP_EOL;
            }

		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();

        try {
            $stmt = $this->db->prepare("SELECT id, title, author, description FROM book");
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($rows as $book) {
                $booklist[] = new Book($book['title'], $book['author'], $book['description'], $book['id']);
            }

        } catch (PDOException $e) {
            $e->getMessage() . PHP_EOL;
        }

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;

        try {
            $stmt = $this->db->prepare("SELECT * FROM book WHERE id=:id");

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $book = new Book($result['title'], $result['author'], $result['description'], $result['id'] );

        } catch (PDOException $e) {
            $e->getMessage() . PHP_EOL;
        }

        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        try {

            $stmt = $this->db->prepare("INSERT INTO book(title, author, description) 
                                                  VALUES (:title, :author, :description)");

            $stmt->bindParam(':title', $book->title, PDO::PARAM_STR);
            $stmt->bindParam(':author', $book->author, PDO::PARAM_STR);
            $stmt->bindParam(':description', $book->description, PDO::PARAM_STR);

            if ($book->title != "" && $book->author != "")
            {
            $stmt->execute();

            } else
                {
                    echo "<font color = red> <h1>Both the title and author of the book must be registered!</h1> </font color = red>";
                }

        } catch (PDOException $e) {
            $e->getMessage() . PHP_EOL;
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        try {



            $stmt = $this->db->prepare("UPDATE book 
                                        SET title=:title, author = :author, description =:description
                                        WHERE id = :id");

            $stmt->bindParam('id', $book->id, PDO::PARAM_INT);
            $stmt->bindParam(':title', $book->title, PDO::PARAM_STR);
            $stmt->bindParam(':author', $book->author, PDO::PARAM_STR);
            $stmt->bindParam(':description', $book->description, PDO::PARAM_STR);

            if ($book->title != "" && $book->author != "")
            {
                $stmt->execute();
            }else{
                echo "<font color = red> <h1>Both the title and author of the book must be registered!</h1> </font color = red>";
            }
        } catch (PDOException $e) {
            $e->getMessage() . PHP_EOL;
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try {

            $stmt = $this->db->prepare("DELETE from book WHERE id = :id");

            $stmt->bindParam(':id', $id, PDO::PARAM_STR);

            $stmt->execute();

        } catch (PDOException $e) {
            $e->getMessage() . PHP_EOL;
        }
    }
	
}

?>